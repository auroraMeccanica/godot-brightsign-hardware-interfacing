extends Node2D

var _my_js_callback = JavaScript.create_callback(self, "_on_serial_received")
var serial = JavaScript.create_object("BSSerialPort", 2)

# Called when the node enters the scene tree for the first time.
func _ready():
	var dev = JavaScript.create_object("BSDeviceInfo")
	serial.SetBaudRate(115200);
	serial.SetDataBits(8);
	serial.SetStopBits(1);
	serial.SetParity("none");
	serial.SetEcho(true);
	
	serial.SetGenerateLineEvent(true);
	serial.onserialline = _my_js_callback

func _on_serial_received(args):
	var data = args[0].sdata.strip_edges()
	if data == "X001A[3]":
		$ColorRect.material.set_shader_param("factor", 1.0)
	else:
		$ColorRect.material.set_shader_param("factor", 0.0)
