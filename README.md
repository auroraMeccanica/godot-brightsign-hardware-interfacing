# Godot + Brightsign hardware template

This template shows how Godot engine can interact with the brightsign hardware.

Godot projects can be exported as HTML5 static files. Brightsign players can display webpages with WebGL, also served from an internal server or from an external website.
Godot projects exported in such a way can also access the Javascript runtime.

Brightsign software exposes simple JS objects to the browser interface to interact with the hardware (i.e. GPIO and Serial communication). These objects are a bridge between the browser and the internal "brightscript" language.

These feature is called [Brighscript-Javascript objects](https://brightsign.atlassian.net/wiki/spaces/DOC/pages/370672351/BrightScript-JavaScript+Objects) and it needs to be manually activated. Simply tick the option inside the HTML widget options inside Brightauthor.
